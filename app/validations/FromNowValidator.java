package validations;

import java.util.Calendar;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.hibernate.validator.internal.constraintvalidators.FutureValidatorForCalendar;


public class FromNowValidator implements ConstraintValidator<FromNow, Calendar> {

	private FutureValidatorForCalendar futureValidator = new FutureValidatorForCalendar();
	
	@Override
	public boolean isValid(Calendar date, ConstraintValidatorContext constraintValidatorContext) {
		if(date == null)
			return true;
		
		Calendar today = Calendar.getInstance();
		return sameDay(today, date) || futureValidator.isValid(date, constraintValidatorContext);
	} 

	private boolean sameDay(Calendar today, Calendar date) {
		return today.get(Calendar.DAY_OF_MONTH) == date.get(Calendar.DAY_OF_MONTH) &&
			   today.get(Calendar.MONTH) == date.get(Calendar.MONTH) &&
			   today.get(Calendar.YEAR) == date.get(Calendar.YEAR);
	}
	
	@Override
	public void initialize(FromNow arg0) {
	}

}
