package persisence;

import models.User;
import play.libs.F.Option;

import com.avaje.ebean.Ebean;

public class Users {

	public static Option<User> findBy(String email, String password) {
		User user = Ebean.find(User.class)
						 .where()
						 .eq("email", email)
						 .eq("password", password)
						 .findUnique();
		
		if (user == null) {
			return Option.<User>None();
		}
		
		return Option.Some(user);
	}
	
}