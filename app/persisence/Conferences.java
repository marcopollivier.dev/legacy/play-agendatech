package persisence;

import java.util.List;

import models.Conference;
import play.libs.F.Option;

import com.avaje.ebean.Ebean;

public class Conferences {

	public static List<Conference> approved(boolean status) {
		return Ebean.find(Conference.class).where()
										   .eq("approved", status)
										   .findList();
	}

	public static Option<Conference> findById(Integer id) {
		Conference conference = Ebean.find(Conference.class)
						 .where()
						 .eq("id", id)
						 .findUnique();
		
		if (conference == null) {
			return Option.<Conference>None();
		}
		
		return Option.Some(conference);
	}
	
}
