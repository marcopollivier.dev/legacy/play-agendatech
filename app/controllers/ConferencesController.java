package controllers;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.avaje.ebean.Ebean;

import models.Conference;
import persisence.Conferences;
import play.api.templates.Html;
import play.data.Form;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import play.mvc.Http.RequestBody;
import play.mvc.Result;
import tools.EmailsSender;
import views.html.conferences.list;
import views.html.conferences.form_conference;

public class ConferencesController extends Controller {

	private static Form<Conference> conferenceForm = Form.form(Conference.class);
	private static boolean isNew = true; //TODO @Marco refatorar - Débito técnico
	
	public static Result list(String message) {
		List<Conference> conferences = Ebean.find(Conference.class).findList();
		return ok(list.render(conferences, message));
	}

	public static Result list() {
		return list("");
	}
	
	public static Result newForm() {
		Form<Conference> formFromRequest = new Form<Conference>(Conference.class); 
		Html view = form_conference.render(formFromRequest);
		return ok(view);
	}

	public static Result editForm(Integer id) {
		Form<Conference> formFromRequest = new Form<Conference>(Conference.class); 
		Option<Conference> conference = Conferences.findById(id);
		
		if(conference.isDefined()) {
			Form<Conference> fill = formFromRequest.fill(conference.get());
			Html view = form_conference.render(fill);
			isNew = false;
			return ok(view);
		}
		
		return list("Erro inesperado: ocorreu um erro ao tentar editar o registro selecionado");
	}

	public static Result delete(Integer id) {
		Option<Conference> conference = Conferences.findById(id);
		
		Conference bean = null;
		if(conference.isDefined()) {
			bean = conference.get(); 
		}
		
		//TODO @Marco deletar imagem
		//TODO @Marco persistir nome da imagem no banco. Acho que agora ela só está gravando em disco
		
		String message = "";
		try {
			Ebean.delete(bean);
			message = "Registro " + bean.getName() + " excluído com sucesso!"; 
		} catch (Exception e) {
			message = "Erro inesperado: Ocorreu um erro ao tentar excluir o registro " + bean.getName();
		}
		
		EmailsSender.notifyNew(bean);
		
		return list("");
	}
	
	public static Result persist() throws IOException { 
		if(isNew)
			return create();
		else
			return update();
	}
	
	private static Result update() throws IOException {
		Form<Conference> formFromRequest = conferenceForm.bindFromRequest();

		if (formFromRequest.hasErrors()) {
			return badRequest(form_conference.render(formFromRequest));
		}
		
		Conference conference = formFromRequest.get();
		
		File featureImage = saveFeatureImage();
		if(featureImage != null)
			conference.setFeatureImageSrc("/assets/uploads/images/feature_images/"+featureImage.getName());
		
		try {
			Ebean.save(conference);
		} catch (Exception e) {
			featureImage.delete();
		}
		
		EmailsSender.notifyNew(conference);
		
		return redirect(routes.ConferencesController.list());
	}

	private static Result create() throws IOException {
		Form<Conference> formFromRequest = conferenceForm.bindFromRequest();

		if (formFromRequest.hasErrors()) {
			return badRequest(form_conference.render(formFromRequest));
		}
		
		Conference conference = formFromRequest.get();
		
		File featureImage = saveFeatureImage();
		if(featureImage != null)
			conference.setFeatureImageSrc("/assets/uploads/images/feature_images/"+featureImage.getName());
		
		try {
			Ebean.save(conference);
		} catch (Exception e) {
			featureImage.delete();
		}
		
		EmailsSender.notifyNew(conference);
		
		return redirect(routes.ConferencesController.list());
	}

	//TODO @Marco refatorar
	private static File saveFeatureImage() throws IOException {
		RequestBody requestBody = request().body();
		MultipartFormData body = requestBody.asMultipartFormData();
		FilePart filePart  = body.getFile("featureImage");
		if(filePart == null)
			return null;
		File featureImageUploaded = filePart.getFile();
		String newFileName = System.currentTimeMillis() + "_" + featureImageUploaded.getName();		
		File newImage = new File("public/uploads/images/feature_images/" + newFileName);
		FileUtils.moveFile(featureImageUploaded, newImage);
		return newImage;
	}

}