package controllers.admin;

import java.util.List;

import models.Conference;
import persisence.Conferences;
import play.mvc.Controller;
import play.mvc.Result;

import com.avaje.ebean.Ebean;

public class AdminConferencesController extends Controller {

	public static Result approvedList() {
		if(session().containsKey("email")) { 
			List<Conference> approved = Conferences.approved(true);
			List<Conference> nonApproved = Conferences.approved(false);
			return ok(views.html.conferences.admin.all_conferences.render(nonApproved,approved));
		}
		return redirect(controllers.routes.LoginController.form());
	}

	public static Result approve(Integer id) {
		Conference conference = Ebean.find(Conference.class, id);
		conference.setApproved(true);
		Ebean.update(conference);
		return redirect(controllers.admin.routes.AdminConferencesController.all());
	}
	
}