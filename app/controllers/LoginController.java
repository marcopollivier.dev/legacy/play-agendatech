package controllers;

import models.User;
import persisence.Users;
import play.data.DynamicForm;
import play.data.DynamicForm.Dynamic;
import play.data.Form;
import play.libs.F.Option;
import play.mvc.Controller;
import play.mvc.Result;
import akka.util.Crypt;

public class LoginController extends Controller {

	private static DynamicForm form = Form.form();
	
	public static Result form() {
		return ok(views.html.login.render(form));
	}
	
	public static Result login() {
		Form<Dynamic> requestForm = form.bindFromRequest();
		String email = requestForm.data().get("email");
		String password = requestForm.data().get("password");
		Option<User> user = Users.findBy(email, Crypt.sha1(password)); 
		
		if(user.isDefined()) {
			session().put("email", user.get().getEmail());
			return redirect(controllers.admin.routes.AdminConferencesController.all());
		}
		
		DynamicForm errorForm = form.fill(requestForm.data());
		errorForm.reject("Login/password invalid");
		return forbidden(views.html.login.render(errorForm));
	}

	public static Result logout() {
		session().clear();
		return ok(views.html.login.render(form));
	}

	
}
