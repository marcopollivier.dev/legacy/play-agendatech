package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.validator.constraints.URL;

import play.data.validation.Constraints.Email;
import play.data.validation.Constraints.Required;
import play.data.validation.ValidationError;
import validations.FromNow;

@Entity
public class Conference {

	@Id
	@GeneratedValue
	private Integer id;
	@Required
	private String name;
	@Column(columnDefinition = "text")
	private String description;

	@Enumerated(EnumType.STRING)
	private ConferenceStateEnum state;

	@Email
	private String contactEmail;
	@URL
	private String site;
	private String twitter;

	@FromNow
	@Column(name="start_date")
	private Calendar startDate;
	@FromNow
	@Column(name="end_date")
	private Calendar endDate;
	
	private boolean approved;
	
	@Column(name="feature_image_src")
	private String featureImageSrc;
	
	//Getters and Setters
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ConferenceStateEnum getState() {
		return state;
	}

	public void setState(ConferenceStateEnum state) {
		this.state = state;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getTwitter() {
		return twitter;
	}

	public void setTwitter(String twitter) {
		this.twitter = twitter;
	}

	public Calendar getStartDate() {
		return startDate;
	}

	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}

	public Calendar getEndDate() {
		return endDate;
	}

	public void setEndDate(Calendar endDate) {
		this.endDate = endDate;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public String getFeatureImageSrc() {
		return featureImageSrc;
	}

	public void setFeatureImageSrc(String featureImageSrc) {
		this.featureImageSrc = featureImageSrc;
	}

	//Validation
	/**
	 * Used by Play
	 * @return
	 */
	public List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<>();
		if (endDate == null) {
			this.endDate = (Calendar) startDate.clone();
			return null;
		}
		if (!endDate.after(startDate)) {
			ValidationError validationError = new ValidationError("endDate","The end must be greater than the beginning");
			errors.add(validationError);
		}
		
		return errors.isEmpty() ? null : errors;
	}
	
}