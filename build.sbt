name := "agendatech"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  "mysql" % "mysql-connector-java" % "5.1.18",
  "commons-io" % "commons-io" % "2.3",
  "javax.mail" % "mail" % "1.4.1",
  "com.typesafe.play" %% "play-mailer" % "2.4.0"
)

play.Project.playJavaSettings
