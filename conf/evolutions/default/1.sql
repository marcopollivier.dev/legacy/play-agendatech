# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table conference (
  id                        integer auto_increment not null,
  name                      varchar(255),
  description               text,
  state                     varchar(2),
  contact_email             varchar(255),
  site                      varchar(255),
  twitter                   varchar(255),
  start_date                datetime,
  end_date                  datetime,
  approved                  tinyint(1) default 0,
  feature_image_src         varchar(255),
  constraint ck_conference_state check (state in ('SP','RJ','MG','ES')),
  constraint pk_conference primary key (id))
;

create table user (
  email                     varchar(255) not null,
  password                  varchar(255),
  constraint pk_user primary key (email))
;




# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table conference;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

