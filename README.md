# Projeto AgendaTech Play

## Proposta
Criar uma solu��o que atenda o m�ximo de recursos utilizados no dia-a-dia de um desenvolvedor web, utilizando os seguintes recursos (com suas respectivas vers�es):

* Play Framework 2.2.6
    - Scala 2.10.3
    - Java 1.8
* Bootstrap 3.3.5-dist

## Motiva��o
- Play: estudar um framework que apresente facilidade ao desenvolvedor na contru��o de aplica��es web; 
- Bootstrap: estudar um framwork que auxilia a constru��o de interfaces gr�ficas para a aplica��o web;
- Git (GitHub): estudar as funcionalidades do controlador de vers�o e consolidar os conhecimentos adquiridos anteriormente. Ao mesmo tempo disponivilizar de forma livre e gratuita o c�digo para consulta da comunidade. 

> Vale ressaltar que no momento em que este projeto teve in�cio, a vers�o corrente do PlayFramework era a 2.4.x. Entretanto... 

## Refer�ncias
Play Framework
- https://www.playframework.com/documentation/2.2.x/Home
- SOUZA, Alberto - Play Framework: Focus on what is really important - 2015 - Editora Code Crushing;
- BOAGLIO, Fernando - Play Framework Java para web sem Servlets e com divers�o - 2014 - Editora Casa do C�digo;

Play2War
- https://github.com/play2war/play2-war-plugin/wiki/Configuration

Bootstrap

Git/GitHub
- https://git-scm.com/book/en/v2
- AQUILES, Alexandre; FERREIRA Rodrigo - Controlando vers�es com Git e GitHub - 2015 - Editora Casa do C�digo;

## Iniciando o projeto (colocar os comandos)

1. Clonar o reposit�rio;
2. Entrar no diret�rio do projeto;
3. Executar o Play
4. Limpar as depend�ncias
5. Compilar o projeto
6. Configurar a IDE
7. Executar a Aplica��o

## Vers�o
Nenhuma vers�o dispon�vel at� o momento.

## Equipe
 - Marco Paulo Ollivier

License
----
MIT